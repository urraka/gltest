#include <GL/glew.h>
#include <GL/glfw.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/random.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtc/type_precision.hpp>

#include <iostream>
#include <vector>
#include <algorithm>
#include <stdint.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <png.h>

using glm::mat4;
using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::ivec2;
using glm::u8vec4;

#define offs(obj, member) (GLvoid*)((size_t)glm::value_ptr((obj).member) - (size_t)&(obj))

struct vertex_t
{
	vec2 position;
	vec2 uv;
	u8vec4 color;
	static vertex_t v;
};

vertex_t vertex_t::v;

uint8_t *load_image(const char *path, int &width, int &height, GLenum &format);
GLuint load_texture(uint8_t *image, int width, int height, GLenum format, bool repeat = false);

std::vector<int> keydown;

void GLFWCALL keyboard(int key, int action)
{
	if (action == GLFW_PRESS)
		keydown.push_back(key);
}

//------------------------------------------------------------------------------
// main
//------------------------------------------------------------------------------

int main()
{
	GLFWvidmode desktopMode;
	ivec2 videoMode(1024, 768);
	mat4 matrix(1.0f), projection = glm::ortho(0.0f, (float)videoMode.x, (float)videoMode.y, 0.0f);

	glfwInit();
	glfwGetDesktopMode(&desktopMode);
	glfwOpenWindowHint(GLFW_OPENGL_VERSION_MAJOR, 2);
	glfwOpenWindowHint(GLFW_OPENGL_VERSION_MINOR, 1);
	glfwOpenWindow(videoMode.x, videoMode.y, 0, 0, 0, 0, 0, 0, GLFW_WINDOW);
	glfwSetWindowPos(desktopMode.Width / 2 - videoMode.x / 2, desktopMode.Height / 2 - videoMode.y / 2);
	glfwSetKeyCallback(keyboard);
	glewInit();

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glViewport(0, 0, videoMode.x, videoMode.y);

	// shader

	const char *vertSource =
		"#version 110\n"
		"uniform mat4 matrix;"
		"uniform mat4 projection;"
		"attribute vec2 position;"
		"attribute vec2 texCoords;"
		"attribute vec4 color;"
		"varying vec2 fragTexCoords;"
		"varying vec4 fragColor;"
		"varying vec2 fragPosition;"
		"void main() {"
		"	fragColor = color;"
		"	fragPosition = position;"
		"	fragTexCoords = texCoords;"
		"	gl_Position = projection * matrix * vec4(position, 0.0, 1.0);"
		"}";
	const char *fragSource =
		"#version 110\n"
		"varying vec2 fragTexCoords;"
		"varying vec4 fragColor;"
		"varying vec2 fragPosition;"
		"uniform sampler2D sampler0;"
		"uniform sampler2D sampler1;"
		"uniform float textureEnabled;"
		"uniform float time;"
		"void main() {"
		"	vec2 displacement = texture2D(sampler1, fragPosition / 512.0 / 10.0).xy;"
		"	float t = fragTexCoords.y + displacement.y * 0.05 - 0.15 + (sin(fragPosition.x / 5.0 + time * 5.0) * 0.0015);"
		"	gl_FragColor = fragColor * texture2D(sampler0, vec2(fragTexCoords.x, t));"
		"	gl_FragColor = mix(fragColor, gl_FragColor, textureEnabled);" // trick to draw background with no water effect
		"}";

	struct
	{
		GLint status;
		GLint length;
		GLchar *info;
	} shaderError = {0};

	GLuint program = glCreateProgram();
	GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);

	glBindAttribLocation(program, 0, "position");
	glBindAttribLocation(program, 1, "texCoords");
	glBindAttribLocation(program, 2, "color");

	glShaderSource(vertShader, 1, &vertSource, 0);
	glShaderSource(fragShader, 1, &fragSource, 0);
	glCompileShader(vertShader);
	glCompileShader(fragShader);
	glAttachShader(program, vertShader);
	glAttachShader(program, fragShader);
	glLinkProgram(program);
	glGetProgramiv(program, GL_LINK_STATUS, &shaderError.status);

	if (shaderError.status == GL_FALSE)
	{
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &shaderError.length);
		shaderError.info = new GLchar[shaderError.length + 1];
		glGetProgramInfoLog(program, shaderError.length, 0, shaderError.info);
		std::cout << shaderError.info << std::endl;
		delete[] shaderError.info;
	}

	glDetachShader(program, vertShader);
	glDetachShader(program, fragShader);
	glDeleteShader(vertShader);
	glDeleteShader(fragShader);
	glUseProgram(program);

	// uniforms

	struct
	{
		GLuint matrix;
		GLuint projection;
		GLuint sampler0;
		GLuint sampler1;
		GLuint textureEnabled;
		GLuint time;
	}
	loc = {
		glGetUniformLocation(program, "matrix"),
		glGetUniformLocation(program, "projection"),
		glGetUniformLocation(program, "sampler0"),
		glGetUniformLocation(program, "sampler1"),
		glGetUniformLocation(program, "textureEnabled"),
		glGetUniformLocation(program, "time")
	};

	glUniform1i(loc.sampler0, 0);
	glUniform1i(loc.sampler1, 1);
	glUniform1f(loc.textureEnabled, 1.0f);
	glUniformMatrix4fv(loc.projection, 1, GL_FALSE, glm::value_ptr(projection));
	glUniformMatrix4fv(loc.matrix, 1, GL_FALSE, glm::value_ptr(matrix));

	// textures

	struct
	{
		GLuint id;
		GLenum format;
		int width;
		int height;
		vec2 size;
		uint8_t *data;
	} texWater = {0}, texDisplacement = {0}, *tex = 0;

	tex = &texWater;
	tex->data = load_image("water1.png", tex->width, tex->height, tex->format);
	tex->id = load_texture(tex->data, tex->width, tex->height, tex->format);
	tex->size = vec2((float)tex->width, (float)tex->height);
	delete[] tex->data;

	tex = &texDisplacement;
	tex->data = load_image("displacement.png", tex->width, tex->height, tex->format);
	tex->id = load_texture(tex->data, tex->width, tex->height, tex->format, true);
	tex->size = vec2((float)tex->width, (float)tex->height);
	delete[] tex->data;

	// water

	struct spring_t
	{
		float velocity;
		float y[2]; // 0-> previous frame, 1-> current frame
	};

	struct
	{
		vec2 position;
		float width;
		float height;
		float resolution;
		int size;
		float dampening;
		float tension;
		float spread;
		spring_t *spring;
		float *lDeltas;
		float *rDeltas;
	} surface = {};

	surface.position = vec2(0.0f, 300.0f); // top-left of the water surface
	surface.width = (float)videoMode.x;
	surface.height = (float)videoMode.y - surface.position.y;
	surface.resolution = 5.0f;
	surface.size = (int)glm::floor(surface.width / surface.resolution) + 2;
	surface.dampening = 0.2f;
	surface.tension = 100.0f;
	surface.spread = 10.0f;
	surface.spring = new spring_t[surface.size]();
	surface.lDeltas = new float[surface.size]();
	surface.rDeltas = new float[surface.size]();

	// water vertex data

	vertex_t *vertWater = new vertex_t[surface.size * 2]; // we need vertices on top and bottom

	// buffer will have all vertices from top first, then all vertices from bottom
	// indices will be used to build the triangles from that
	// that way the bottom vertices don't have to be uploaded each frame

	for (size_t i = 0; i < surface.size; i++)
	{
		// top
		vertWater[i].position = vec2(glm::min(surface.position.x + surface.width, surface.position.x + i * surface.resolution), surface.position.y);
		vertWater[i].uv = vec2(i * surface.resolution / surface.width, 0.0f);
		vertWater[i].color = u8vec4(255, 255, 255, 128); // top with 0.5 alpha

		// bottom
		vertWater[i + surface.size].position = vec2(vertWater[i].position.x, surface.position.y + surface.height);
		vertWater[i + surface.size].uv = vec2(vertWater[i].uv.x, 1.0f);
		vertWater[i + surface.size].color = u8vec4(255, 255, 255, 128); // bottom with full opacity
	}

	// water index buffer data

	uint16_t *indicesWater = new uint16_t[(surface.size - 1) * 6]; // number of quads: surface.size - 1; indices per quad: 6

	for (size_t iVert = 0, i = 0; iVert < surface.size - 1; iVert++, i += 6)
	{
		indicesWater[i + 0] = iVert;
		indicesWater[i + 1] = iVert + 1;
		indicesWater[i + 2] = iVert + surface.size;

		indicesWater[i + 3] = iVert + 1;
		indicesWater[i + 4] = iVert + surface.size + 1;
		indicesWater[i + 5] = iVert + surface.size;
	}

	// create buffer and upload vertices

	GLuint vboWater;
	glGenBuffers(1, &vboWater);
	glBindBuffer(GL_ARRAY_BUFFER, vboWater);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_t) * surface.size * 2, vertWater, GL_DYNAMIC_DRAW);

	// create index buffer

	GLuint iboWater;
	glGenBuffers(1, &iboWater);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, iboWater);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint16_t) * (surface.size - 1) * 6, indicesWater, GL_STATIC_DRAW);

	// create buffer for background

	vertex_t background[4] = {};

	background[0].position = vec2(0.0f, 0.0f);
	background[1].position = vec2(1024.0f, 0.0f);
	background[2].position = vec2(1024.0f, 768.0f);
	background[3].position = vec2(0.0f, 768.0f);
	background[0].color = u8vec4(0, 0, 255, 255);
	background[1].color = u8vec4(0, 0, 255, 255);
	background[2].color = u8vec4(255, 255, 255, 255);
	background[3].color = u8vec4(255, 255, 255, 255);

	GLuint vboBackground;
	glGenBuffers(1, &vboBackground);
	glBindBuffer(GL_ARRAY_BUFFER, vboBackground);
	glBufferData(GL_ARRAY_BUFFER, sizeof(background), background, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texWater.id);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, texDisplacement.id);

	uint64_t step = 0;
	const double dt = 0.03;
	double t = 0.0;
	double currentTime = glfwGetTime();
	double accumulator = 0.0;

	while (glfwGetWindowParam(GLFW_OPENED) == GL_TRUE)
	{
		double newTime = glfwGetTime();
		double frameTime = glm::min(newTime - currentTime, 0.25);

		currentTime = newTime;
		accumulator += frameTime;

		if (std::find(keydown.begin(), keydown.end(), GLFW_KEY_SPACE) != keydown.end())
			surface.spring[rand() % surface.size].velocity = 1500.0f;

		keydown.clear();

		while (accumulator >= dt)
		{
			for (size_t i = 0; i < surface.size; i++)
			{
				spring_t &spring = surface.spring[i];
				float &y = spring.y[1];
				spring.y[0] = y;

				float acc = -surface.tension * y - surface.dampening * spring.velocity;
				spring.velocity += acc * (float)dt;
				y += spring.velocity * (float)dt;
			}

			for (size_t i = 0; i < surface.size; i++)
			{
				if (i > 0)
				{
					surface.lDeltas[i] = surface.spread * (surface.spring[i].y[1] - surface.spring[i - 1].y[1]);
					surface.spring[i - 1].velocity += surface.lDeltas[i];
				}

				if (i < surface.size - 1)
				{
					surface.rDeltas[i] = surface.spread * (surface.spring[i].y[1] - surface.spring[i + 1].y[1]);
					surface.spring[i + 1].velocity += surface.rDeltas[i];
				}
			}

			for (size_t i = 0; i < surface.size; i++)
			{
				if (i > 0)
					surface.spring[i - 1].y[1] += surface.lDeltas[i] * dt;

				if (i < surface.size - 1)
					surface.spring[i + 1].y[1] += surface.rDeltas[i] * dt;
			}

			t += dt;
			step++;
			accumulator -= dt;
		}

		float percent = (float)(accumulator / dt);

		glClear(GL_COLOR_BUFFER_BIT);

		// draw background
		glUniform1f(loc.textureEnabled, 0.0f);
		glBindBuffer(GL_ARRAY_BUFFER, vboBackground);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(vertex_t), offs(vertex_t::v, position));
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(vertex_t), offs(vertex_t::v, uv));
		glVertexAttribPointer(2, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(vertex_t), offs(vertex_t::v, color));
		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

		// update water vertices (top vertices only)
		for (size_t i = 0; i < surface.size; i++)
		{
			float y = glm::mix(surface.spring[i].y[0], surface.spring[i].y[1], percent);
			vertWater[i].position.y = surface.position.y + y;
		}

		// upload updated vertices
		glBindBuffer(GL_ARRAY_BUFFER, vboWater);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertex_t) * surface.size, vertWater);

		// draw water
		glUniform1f(loc.time, glm::mod((float)(t + accumulator) * 3.0f, 360.0f)); // 360 because it's just used for sin()
		glUniform1f(loc.textureEnabled, 1.0f);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, iboWater);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(vertex_t), offs(vertex_t::v, position));
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(vertex_t), offs(vertex_t::v, uv));
		glVertexAttribPointer(2, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(vertex_t), offs(vertex_t::v, color));
		glDrawElements(GL_TRIANGLES, (surface.size - 1) * 6, GL_UNSIGNED_SHORT, (GLvoid*)0);

		glfwSwapBuffers();

		if (glfwGetKey(GLFW_KEY_ESC) == GLFW_PRESS)
			break;
	}

	delete[] surface.spring;
	delete[] surface.lDeltas;
	delete[] surface.rDeltas;
	delete[] vertWater;
	delete[] indicesWater;

	glDeleteProgram(program);
	glDeleteBuffers(1, &vboWater);
	glDeleteBuffers(1, &iboWater);
	glDeleteBuffers(1, &vboBackground);
	glDeleteTextures(1, &texWater.id);
	glDeleteTextures(1, &texDisplacement.id);
	glfwTerminate();
}

//------------------------------------------------------------------------------
// load_image
//------------------------------------------------------------------------------

uint8_t *load_image(const char *path, int &width, int &height, GLenum &format)
{
	FILE *file = fopen(path, "rb");

	if (!file)
	{
		std::cerr << "load_image(" << path << ") - Failed to open file." << std::endl;
		return 0;
	}

	size_t headerSize = 8;
	uint8_t header[8];
	headerSize = fread(header, 1, headerSize, file);

	if (png_sig_cmp(header, 0, headerSize))
	{
		std::cerr << "load_image(" << path << ") - File is not PNG." << std::endl;
		fclose(file);
		return 0;
	}

	png_structp png = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

	if (!png)
	{
		std::cerr << "load_image(" << path << ") - png_create_read_struct() failed." << std::endl;
		fclose(file);
		return 0;
	}

	png_infop info = png_create_info_struct(png);

	if (!info)
	{
		std::cerr << "load_image(" << path << ") - First png_create_info_struct() failed." << std::endl;
		png_destroy_read_struct(&png, NULL, NULL);
		fclose(file);
		return 0;
	}

	png_infop info_end = png_create_info_struct(png);

	if (!info_end)
	{
		std::cerr << "load_image(" << path << ") - Second png_create_info_struct() failed." << std::endl;
		png_destroy_read_struct(&png, &info, NULL);
		fclose(file);
		return 0;
	}

	if (setjmp(png_jmpbuf(png)))
	{
		std::cerr << "load_image(" << path << ") - setjmp() failed." << std::endl;
		png_destroy_read_struct(&png, &info, &info_end);
		fclose(file);
		return 0;
	}

	png_init_io(png, file);
	png_set_sig_bytes(png, headerSize);
	png_read_png(png, info, PNG_TRANSFORM_STRIP_16 | PNG_TRANSFORM_PACKING | PNG_TRANSFORM_EXPAND, NULL);

	int color = png_get_color_type(png, info);
	width = png_get_image_width(png, info);
	height = png_get_image_height(png, info);

	if (color != PNG_COLOR_TYPE_RGB && color != PNG_COLOR_TYPE_RGB_ALPHA)
	{
		std::cerr << "load_image(" << path << ") - Image must be RGB or RGBA." << std::endl;
		png_destroy_read_struct(&png, &info, &info_end);
		fclose(file);
		return 0;
	}

	png_bytep *rows = png_get_rows(png, info);

	format = (color == PNG_COLOR_TYPE_RGB_ALPHA ? GL_RGBA : GL_RGB);
	int channels = (format == GL_RGB ? 3 : 4);
	uint8_t *image = new uint8_t[width * height * channels];

	for (int y = 0; y < height; y++)
		memcpy(image + y * width * channels, rows[y], width * channels);

	png_destroy_read_struct(&png, &info, &info_end);
	fclose(file);

	return image;
}

//------------------------------------------------------------------------------
// load_texture
//------------------------------------------------------------------------------

GLuint load_texture(uint8_t *image, int width, int height, GLenum format, bool repeat)
{
	GLuint id;

	glGenTextures(1, &id);
	glBindTexture(GL_TEXTURE_2D, id);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, 0);

	int channels = (format == GL_RGB ? 3 : 4);

	for (int y = 0; y < height; y++)
		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, y, width, 1, format, GL_UNSIGNED_BYTE, image + y * width * channels);

	GLint mode = repeat ? GL_REPEAT : GL_CLAMP_TO_EDGE;

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, mode);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, mode);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	return id;
}
